use gettextrs::*;
use gio::prelude::*;
use gtk::prelude::*;

mod config;
mod window;
mod state;
mod utils;
mod ciphers;
use crate::window::{Window, About};
use crate::utils::Cipher;
use crate::state::CipherState;
use std::sync::{Arc, Mutex};


#[macro_use]
extern crate lazy_static;

fn main() {
    gtk::init().unwrap_or_else(|_| panic!("Failed to initialize GTK."));

    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain("decypher", config::LOCALEDIR);
    textdomain("decypher");

    let res = gio::Resource::load(config::PKGDATADIR.to_owned() + "/decypher.gresource")
        .expect("Could not load resources");
    gio::resources_register(&res);

    let app = gtk::Application::new(Some("com.mateusrodcosta.Decypher"), Default::default()).unwrap();

    let win = Window::new();
    let window = win.clone();
    app.connect_activate(move |app| {

        let cipher_state = Arc::new(Mutex::new(CipherState::new()));

        window.widget.set_application(Some(app));
        app.add_window(&window.widget);
        window.widget.present();

        {
            let combo_cipher = window.header_bar.combo_cipher.clone();

            let cipher_state = Arc::clone(&cipher_state);

            let header_bar_stack = window.header_bar.header_bar_aux.widget.clone();

            combo_cipher.connect_changed(move |widget| {
                let id = widget.get_active_id();
                let cipher = determine_cipher(id);
                match cipher {
                    Cipher::Caesar => {
                        let page_shift = header_bar_stack.get_child_by_name("page_shift").unwrap();
                        header_bar_stack.set_visible_child(&page_shift);
                    },
                    Cipher::Vigenere => {
                        let page_keyword = header_bar_stack.get_child_by_name("page_keyword").unwrap();
                        header_bar_stack.set_visible_child(&page_keyword);
                    },
                    _=> {
                        let page_empty = header_bar_stack.get_child_by_name("page_empty").unwrap();
                        header_bar_stack.set_visible_child(&page_empty);
                    }
                }
                cipher_state.lock().unwrap().set_cipher(cipher);
            });
        }
        {
            let spin_button_shift = window.header_bar.header_bar_aux.spin_button_shift.clone();
            let cipher_state = Arc::clone(&cipher_state);

            spin_button_shift.clone().connect_value_changed(move |widget| {
                let v: f64 = widget.get_value();
                let v = v as usize;
                cipher_state.lock().unwrap().set_shift(v);
            });
        }

        let entry_source = window.content.entry_source.clone();
        let entry_result = window.content.entry_result.clone();
        let entry_key = window.header_bar.header_bar_aux.entry_key.clone();

        let button_encrypt = window.content.button_encrypt.clone();
        let button_decrypt = window.content.button_decrypt.clone();

        {
            let cipher_state = Arc::clone(&cipher_state);
            let entry_source = entry_source.clone();
            let entry_result = entry_result.clone();
            let entry_key = entry_key.clone();
            button_encrypt.clone().connect_clicked(move |_| {
                let s = entry_source.clone().get_text().unwrap();
                let mut data = cipher_state.lock().unwrap();
                data.set_source_text(s.as_str());
                let key = entry_key.get_text().unwrap();
                data.set_key(key.as_str());
                data.encrypt();
                entry_result.set_text(&data.get_result_text());
            });
        }
        {
            let cipher_state = Arc::clone(&cipher_state);
            let entry_source = entry_source.clone();
            let entry_result = entry_result.clone();
            let entry_key = entry_key.clone();
            button_decrypt.connect_clicked(move |_| {
                let s = entry_source.get_text().unwrap();
                let mut data = cipher_state.lock().unwrap();
                data.set_source_text(s.as_str());
                let key = match entry_key.get_text() {
                    Some(x) => x.as_str().to_string(),
                    None => "".to_string(),
                };
                data.set_key(&key);
                data.decrypt();
                entry_result.set_text(&data.get_result_text());
            });
        }
    });

    let window = win.clone();
    let action = gio::SimpleAction::new("about", None);
    action.connect_activate(move |_, _| {
        let about = About::new();
        about.widget.present();
        about.widget.set_transient_for(Some(&window.widget));
    });
    app.add_action(&action);

    let ret = app.run(&std::env::args().collect::<Vec<_>>());
    std::process::exit(ret);
}

fn determine_cipher(val: Option<glib::GString>) -> Cipher {
    match val {
        None => Cipher::None,
        Some(x) =>  match x.as_str() {
            "cipher_caesar" => Cipher::Caesar,
            "cipher_atbash" => Cipher::Atbash,
            "cipher_vigenere" => Cipher::Vigenere,
            "cipher_rot13" => Cipher::Rot13,
            &_ => Cipher::None,
        }
    }
}
