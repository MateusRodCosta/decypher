use crate::utils::*;

pub trait MappingCipher {
    fn decode_char(&self, input: char) -> char;
    fn run(&self, source_text: &str) -> String {
        source_text.chars()
            .map(|c| self.decode_char(c))
            .collect()
    }
}

pub struct TableCipher {
    input_table: String,
    output_table: String
}

impl TableCipher {
    pub fn new(input_table: &str, output_table: &str) -> Self {
        assert_eq!(input_table.len(), output_table.len());
        Self { input_table: String::from(input_table), output_table: String::from(output_table) }
    }
}

impl MappingCipher for TableCipher {
    fn decode_char(&self, input: char) -> char {
        self.input_table.chars().position(|c| c == input)
            .and_then(|i| self.output_table.chars().nth(i))
            .unwrap_or(input)
    }
}


pub struct CaesarCipher {
    shift: u8,
}

impl CaesarCipher {
    pub fn new (shift: usize) -> Self {
        let shift = shift as u8;
        Self { shift }
    }

    fn encode_char(&self, input: char) -> char {
        let val = match ascii_val_from_a(input) {
            None => return input,
            Some(x) => x,
        };
        let r = match input.is_uppercase() {
            true => (val + self.shift) % 26 + 65,
            false => (val + self.shift) % 26 + 97,
        };
        r as char
    }

    fn decode_char(&self, input: char) -> char {
        let val = match ascii_val_from_a(input) {
            None => return input,
            Some(x) => x,
        };
        let r = match input.is_uppercase() {
            true =>  (val + 26 - self.shift) % 26 + 65,
            false => (val + 26 - self.shift) % 26 + 97,
        } as u8;
        r as char
    }

    pub fn encrypt(&self, source_text: &str) -> String {
        source_text.chars()
            .map(|c| self.encode_char(c))
            .collect()
    }

    pub fn decrypt(&self, source_text: &str) -> String {
        source_text.chars()
            .map(|c| self.decode_char(c))
            .collect()
    }
}

pub struct VigenereCipher {
    key: String,
    key_length: usize,
}

impl VigenereCipher {

    pub fn new(key: &str) -> Self {
        Self { key: String::from(key), key_length: key.len() }
    }

    pub fn encrypt(&self, source_text: &str) -> String {
        let mut i = 0;
        source_text.chars().map(|c| {
            let v = match ascii_val_from_a(c) {
                None => return c,
                Some(x) => x,
            };
            let ci = ascii_val_from_a(self.key.chars().nth(i).unwrap()).unwrap();
            let r = (v + ci) % 26;
            let r = match c.is_uppercase() {
                true => r + 65,
                false => r + 97,
            };
            i += 1;
            if i >= self.key_length { i = 0; }
           r as char
        }).collect()
    }

    pub fn decrypt(&self, source_text: &str) -> String {
        let mut i = 0;
        source_text.chars().map(|c| {
            let v = match ascii_val_from_a(c) {
                None => return c,
                Some(x) => x,
            };
            let ci = ascii_val_from_a(self.key.chars().nth(i).unwrap()).unwrap();
            let r = (v + 26 - ci) % 26;
            let r = match c.is_uppercase() {
                true => r + 65,
                false => r + 97,
            };
            i += 1;
            if i >= self.key_length { i = 0; }
           r as char
        }).collect()
    }


}
