use gtk::prelude::*;

#[derive(Clone)]
pub struct Window {
    pub widget: gtk::ApplicationWindow,
    pub header_bar: HeaderBar,
    pub content: Content,
}

impl Window {
    pub fn new() -> Self {
        let builder = gtk::Builder::new_from_resource("/com/mateusrodcosta/Decypher/window.ui");
        let widget: gtk::ApplicationWindow = builder
            .get_object("window")
            .expect("Failed to find the window object");

        let header_bar = HeaderBar::new(&builder);
        let content = Content::new(&builder);

        Self { widget, header_bar, content }
    }
}

#[derive(Clone)]
pub struct HeaderBar {
    pub widget: gtk::HeaderBar,
    pub combo_cipher: gtk::ComboBox,
    pub header_bar_aux: HeaderBarAux,
}

impl HeaderBar {
    pub fn new(builder: &gtk::Builder) -> Self {
        let widget: gtk::HeaderBar = builder
            .get_object("header_bar")
            .expect("Failed to find the header_bar object");
        let combo_cipher: gtk::ComboBox = builder
            .get_object("combo_cipher")
            .expect("Failed to find the combo_cipher object");

        let header_bar_aux = HeaderBarAux::new(&builder);
        Self { widget, combo_cipher, header_bar_aux }
    }
}

#[derive(Clone)]
pub struct HeaderBarAux{
    pub widget: gtk::Stack,
    pub spin_button_shift: gtk::SpinButton,
    pub entry_key: gtk::Entry,
}

impl HeaderBarAux {
    pub fn new(builder: &gtk::Builder) -> Self {
        let widget: gtk::Stack = builder
            .get_object("header_bar_stack")
            .expect("Failed to find the box_shift object");
        let spin_button_shift: gtk::SpinButton = builder
            .get_object("spin_button_shift")
            .expect("Failed to find the spin_button_shift object");
        let entry_key: gtk::Entry = builder
            .get_object("entry_key")
            .expect("Failed to find the entry_key object");
        Self { widget, spin_button_shift, entry_key }
    }
}

#[derive(Clone)]
pub struct Content {
    pub widget: gtk::Box,
    pub entry_source: gtk::Entry,
    pub entry_result: gtk::Entry,
    pub button_encrypt: gtk::Button,
    pub button_decrypt: gtk::Button,
}

impl Content {
    pub fn new(builder: &gtk::Builder) -> Self {
        let widget: gtk::Box = builder
            .get_object("content")
            .expect("Failed to find the content object");
        let entry_source: gtk::Entry = builder
            .get_object("entry_source")
            .expect("Failed to find the entry_source object");
        let entry_result: gtk::Entry = builder
            .get_object("entry_result")
            .expect("Failed to find the entry_result object");
        let button_encrypt: gtk::Button = builder
            .get_object("button_encrypt")
            .expect("Failed to find the button_encrypt object");
        let button_decrypt: gtk::Button = builder
            .get_object("button_decrypt")
            .expect("Failed to find the button_decrypt object");
        Self { widget, entry_source, entry_result, button_encrypt, button_decrypt }
    }
}

#[derive(Clone)]
pub struct About {
    pub widget: gtk::AboutDialog,
}

impl About {
    pub fn new() -> Self {
        let builder = gtk::Builder::new_from_resource("/com/mateusrodcosta/Decypher/window.ui");
        let widget: gtk::AboutDialog = builder
            .get_object("about_dialog")
            .expect("Failed to find the about_dialog object");

        Self { widget }
    }
}
