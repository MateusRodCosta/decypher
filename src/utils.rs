#[derive(Debug)]
pub enum Cipher { None, Caesar, Atbash, Vigenere, Rot13 }

pub fn ascii_val_from_a (input: char) -> Option<u8> {
    if !input.is_ascii_alphabetic() { return None; }
    let byte = input as u8;
    Some (match input.is_uppercase() {
        true => byte - 65,
        false => byte - 97,
    })
}
