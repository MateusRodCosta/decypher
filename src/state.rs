use crate::utils::Cipher;
use crate::ciphers::*;


pub struct CipherState {
    current_cipher: Cipher,
    source_text: String,
    result_text: String,
    shift: usize,
    key: String,
}
lazy_static! {
    static ref ROT13_CIPHER: TableCipher = TableCipher::new(
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
        "NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm",
    );
    static ref ATBASH_CIPHER: TableCipher = TableCipher::new(
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
        "ZYXWVUTSRQPONMLKJIHGFEDCBAzyxwvutsrqponmlkjihgfedcba",
    );
}

impl CipherState {
    pub fn new() -> CipherState {
        CipherState {
            current_cipher: Cipher::None,
            source_text: String::new(),
            result_text: String::new(),
            shift: 3,
            key: String::new(),
        }
    }

    pub fn set_cipher(&mut self, new_cypher: Cipher) {
        self.current_cipher = new_cypher;
    }

    pub fn set_shift(&mut self, shift: usize) {
        self.shift = shift;
    }

    pub fn set_source_text(&mut self, source_text: &str) {
        self.source_text = String::from(source_text);
    }

    pub fn get_result_text(&self) -> String {
        self.result_text.to_owned()
    }

    pub fn set_key(&mut self, key: &str) {
        self.key = String::from(key);
    }

    pub fn encrypt(&mut self) {
        self.result_text = match self.current_cipher {
            Cipher::Rot13 => {
                ROT13_CIPHER.run(&self.source_text)
            },
            Cipher::Atbash => {
                ATBASH_CIPHER.run(&self.source_text)
            },
            Cipher::Caesar => {
                let caesar_cipher = CaesarCipher::new(self.shift);
                caesar_cipher.encrypt(&self.source_text)
            },
            Cipher::Vigenere => {
            println!("{:?}", &self.key);
                if !self.key.is_empty() {
                    let vigenere_cipher = VigenereCipher::new(&self.key);
                    vigenere_cipher.encrypt(&self.source_text)
                } else {
                    String::from("Unavailable")
                }
            },
            _=> String::from("Unavailable"),
        };
    }

    pub fn decrypt(&mut self) {
        self.result_text = match self.current_cipher {
            Cipher::Rot13 => {
                ROT13_CIPHER.run(&self.source_text)
            },
            Cipher::Atbash => {
                ATBASH_CIPHER.run(&self.source_text)
            },
            Cipher::Caesar => {
                let caesar_cipher = CaesarCipher::new(self.shift);
                caesar_cipher.decrypt(&self.source_text)
            },
            Cipher::Vigenere => {
                if !self.key.is_empty() {
                    let vigenere_cipher = VigenereCipher::new(&self.key);
                    vigenere_cipher.decrypt(&self.source_text)
                } else {
                    String::from("Unavailable")
                }
            }
            _=> String::from("Unavailable"),
        }
    }
}
