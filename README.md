# Decypher

A simple GTK program allwoing encrypting and decrypting on the following 4 ciphers: Caesar, Atbash, Vigenere, Rot13.

![Screenshot of Decypher](data/screenshots/decypher.png)

Use GNOME Builder to compile and run.
